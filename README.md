# Conway's Game of Life

This is a webapp that allows the user to see the game of life as in the Conway's definition, although it is highly customizable.
The webapp fills the screen with 20px blocks by default, but the user can set:

- (dev) The game's starting configuration (you click to switch each block in the grid)
- Block Size
- Grid Proportion (quantity of blocks)
- Epoch Frequency
- (dev) Load and save setups

The app is deployed to Vercel as [The Production Version](https://conway-s-game-of-life-plum.vercel.app/)

# About the game

Conway's Game of life is a simulation that runs indefinetly, and the rules are as follows:

- Any live cell with fewer than two live neighbours dies, as if by underpopulation.
- Any live cell with two or three live neighbours lives on to the next generation.
- Any live cell with more than three live neighbours dies, as if by overpopulation.
- Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

For more information about the concept and the science, visit [The Conway's Game of Life Wikipedia Page](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)

# Technologies Used

- Web Components, Custom Elements and Modern JavaScript

## Screenshots

Here is a gif to show how the app runs the game.

![](https://imgur.com/Rjjl3rA.gif)

And a the game itself with a blank slate.

![](https://i.imgur.com/6ITvJAi.png)

## Setup:

Just Launch Live server and you're good to go!
