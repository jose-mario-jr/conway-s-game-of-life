export default {
  light: ["#f5f5f5", "#800080", "#460646", "#dda2dd"],
  dark: ["#0b0b0b", "#460646", "#e4b7f3", "#dda2dd"],
}
