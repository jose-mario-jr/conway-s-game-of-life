class PlayButton extends HTMLElement {
  constructor() {
    super()
    this.active = false
  }

  connectedCallback() {
    this.addEventListener("click", this.onClick)
  }

  onClick(e) {
    this.active = this.active ? this.pause() : this.play()
  }

  play() {
    this.firstElementChild.src = "assets/pause.svg"
    document.querySelector("grid-component").simulate()
    return true
  }
  pause() {
    this.firstElementChild.src = "assets/play.svg"
    document.querySelector("grid-component").pauseSimulation()
    return false
  }
}

customElements.define("play-button-component", PlayButton)
export default PlayButton
