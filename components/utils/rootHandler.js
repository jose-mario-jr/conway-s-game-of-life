class RootHandler {
  constructor() {
    this.rootStyle = document.documentElement.style
  }

  setLines(newLines) {
    this.rootStyle.setProperty("--lines", newLines)
  }
  setColumns(newColumns) {
    this.rootStyle.setProperty("--columns", newColumns)
  }
  setBlHeight(newBlHeight) {
    this.rootStyle.setProperty("--bl-height", newBlHeight + "px")
  }

  setColors([newBgColor, newPrimaryColor, newSecondaryColor, newHoverColor]) {
    this.rootStyle.setProperty("--bg-color", newBgColor)
    this.rootStyle.setProperty("--primary-color", newPrimaryColor)
    this.rootStyle.setProperty("--secondary-color", newSecondaryColor)
    this.rootStyle.setProperty("--hover-color", newHoverColor)
  }
}

export default RootHandler
